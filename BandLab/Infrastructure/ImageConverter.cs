using System.Drawing;
using System.IO;

namespace BandLab.Infrastructure
{
    public class ImageConverter
    {
        public static void ConvertToJpg(Stream source, Stream target)
        {
            Image png = Image.FromStream(source);
            png.Save(target, System.Drawing.Imaging.ImageFormat.Jpeg);
            png.Dispose();
        }
    }
}