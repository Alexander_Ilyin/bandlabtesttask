using System;

namespace BandLab.Infrastructure
{
    public class IndexFormatHelper
    {
        public static string FormatDate(DateTimeOffset timeOffset)
        {
            var s = timeOffset.ToUnixTimeMilliseconds().ToString();
            return s;
        }

        public static string FormatNumber(int commentCount)
        {
            var s = commentCount.ToString();
            return new string('0', 7 - s.Length) + s;
        }
    }
}