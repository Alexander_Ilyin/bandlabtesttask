using System;

namespace BandLab.Infrastructure
{
    public abstract class ModelBase
    {

        public DateTimeOffset CreatedAt { get; set; } = DateTimeOffset.UtcNow;
        
        public string Type => this.GetType().Name;
        public abstract string PartitionKey {get;}
    }
}