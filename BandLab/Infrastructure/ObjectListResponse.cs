using System.Collections.Generic;

namespace BandLab.Infrastructure
{
    public class ObjectListResponse<T>
    {
        public List<T> Items { get; set; } = new List<T>();
        public string NextPageToken { get; set; }
    }
}