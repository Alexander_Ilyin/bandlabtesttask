using Microsoft.Azure.Cosmos;

namespace BandLab.Infrastructure
{
    public class Repository<T> where T : ModelBase
    {
        private readonly CosmosClient cosmosClient;
        private Container container;

        public CosmosClient CosmosClient => cosmosClient;

        public Container Container => container;

        public Repository(CosmosClient cosmosClient)
        {
            this.cosmosClient = cosmosClient;
            this.container = cosmosClient.GetContainer("postDb", "Entities");
        }
        
        
    }
}