using System.Threading.Tasks;
using System.Web.Http;
using BandLab.Infrastructure;
using BandLab.Modules.Posts.Dto.User;
using BandLab.Modules.Posts.Services;

namespace BandLab.Modules.Posts.Controllers
{
    public class AccountController : ApiControllerBase
    {
        private readonly UserService userService;

        public AccountController(UserService userService)
        {
            this.userService = userService;
        }

        [HttpPost]
        [Route("/")]
        public Task<UserResponseItem> CreateUser([FromBody]UserCreateRequest request)
        {
            return userService.CreateUser(GetCurrentUserId(), request);
        }
    }
}