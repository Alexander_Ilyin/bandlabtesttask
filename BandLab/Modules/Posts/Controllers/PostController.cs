using System.Threading.Tasks;
using System.Web.Http;
using BandLab.Infrastructure;
using BandLab.Modules.Posts.Dto;
using BandLab.Modules.Posts.Dto.Comment;
using BandLab.Modules.Posts.Dto.Post;
using BandLab.Modules.Posts.Services;

namespace BandLab.Modules.Posts.Controllers
{
    public class PostController : ApiControllerBase
    {
        private readonly PostService postService;

        public PostController(PostService postService)
        {
            this.postService = postService;
        }

        [HttpPost]
        [Route("/")]
        public Task<PostResponseItem> CreatePost([FromBody]PostCreateRequest request)
        {
            return postService.CreatePost(GetCurrentUserId(), request);
        }

        [HttpGet]
        [Route("/list")]
        public Task<ObjectListResponse<PostResponseItem>> FindPosts([FromUri]PostFindRequest request)
        {
            return postService.GetPosts( request);
        }
        
        [HttpPost]
        [Route("/{postId}/comments")]
        public Task<CommentResponseItem> CreateComment([FromBody]CommentCreateRequest request, string postId)
        {
            request.PostId = postId;
            return postService.CreateComment(GetCurrentUserId(), request);
        }
        
        [HttpGet]
        [Route("/{postId}/comments")]
        public Task<ObjectListResponse<CommentResponseItem>> GetComments([FromUri]CommentFindRequest request, string postId)
        {
            request.PostId = postId;
            return postService.GetComments(request);
        }
    }
}