using System.Threading.Tasks;
using System.Web.Http;
using BandLab.Modules.Posts.Services;

namespace BandLab.Modules.Posts.Controllers
{
    public class UploadController : ApiController
    {
        private readonly UploadService uploadService;

        public UploadController(UploadService uploadService)
        {
            this.uploadService = uploadService;
        }

        [HttpPost]
        public async Task<string> Upload()
        {
            var body =await ControllerContext.Request.Content.ReadAsStreamAsync();
            var image = await uploadService.AddImage(body);
            return image;
        }
    }
}