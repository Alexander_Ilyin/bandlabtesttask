namespace BandLab.Modules.Posts.Dto.Comment
{
    public class CommentCreateRequest
    {
        public string Id { get; set; }
        public string PostId { get; set; }
        public string Text { get; set; }
        
    }
}