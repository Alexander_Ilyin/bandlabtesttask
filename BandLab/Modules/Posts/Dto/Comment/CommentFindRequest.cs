namespace BandLab.Modules.Posts.Dto.Comment
{
    public class CommentFindRequest
    {
        public string PostId { get; set; }
        public string NextPageToken { get; set; }
    }
}