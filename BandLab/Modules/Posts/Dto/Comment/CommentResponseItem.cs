namespace BandLab.Modules.Posts.Dto.Comment
{
    public class CommentResponseItem
    {
        public string Id { get; set; }
        public string AuthorId { get; set; }
        public string PostId { get; set; }
        public string Text { get; set; }

        public static CommentResponseItem From( Model.Comment comment)
        {
            return new CommentResponseItem()
            {
                Id = comment.Id,
                Text = comment.Text,
                AuthorId = comment.AuthorId,
                PostId = comment.PostId
            };
        }
    }
}