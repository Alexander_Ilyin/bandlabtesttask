using System.Collections.Generic;

namespace BandLab.Modules.Posts.Dto.Post
{
    public class PostCreateRequest
    {
        public string Text { get; set; }
        public List<string> UploadImageIds { get; set; } = new List<string>();
    }
}