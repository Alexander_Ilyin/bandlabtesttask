namespace BandLab.Modules.Posts.Dto.Post
{
    public class PostFindRequest
    {
        public string AuthorId { get; set; }
        public string NextPageToken { get; set; }
    }
}