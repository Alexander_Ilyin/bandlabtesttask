using System.Collections.Generic;
using System.Linq;
using BandLab.Modules.Posts.Dto.Comment;

namespace BandLab.Modules.Posts.Dto.Post
{
    public class PostResponseItem
    {
        public string Id { get; set; }
        public List<CommentResponseItem> LastComments { get; set; } = new List<CommentResponseItem>();

        public static PostResponseItem From(Model.Post post)
        {
            return new PostResponseItem()
            {
                Id = post.Id,
                LastComments = post.LastComments.Select(CommentResponseItem.From).ToList()
            };
        }
    }
}