namespace BandLab.Modules.Posts.Dto.User
{
    public class UserResponseItem
    {
        public string Id { get; set; }
        public string Email { get; set; }

        public static UserResponseItem From(Model.User user)
        {
            return new UserResponseItem()
            {
                Id = user.Id,
                Email = user.Email
            };
        }
    }
}