using System.Threading.Tasks;
using BandLab.Infrastructure;
using BandLab.Modules.Posts.Dto;
using BandLab.Modules.Posts.Dto.Comment;
using BandLab.Modules.Posts.Model;
using Microsoft.Azure.Cosmos;

namespace BandLab.Modules.Posts.Logic
{
    public class CommentRepository : Repository<Comment>
    {
        private readonly UserRepository userRepository;

        public CommentRepository(CosmosClient cosmosClient, UserRepository userRepository) : base(cosmosClient)
        {
            this.userRepository = userRepository;
        }
        
        public async Task Create(Comment comment)
        {
            await Container.UpsertItemAsync(comment, new PartitionKey(comment.PartitionKey));
        }
        
        public async Task<ObjectListResponse<Comment>> GetComments(CommentFindRequest commentFindRequest)
        {
            QueryDefinition query = new QueryDefinition(@"select * from Entities c  
            where 
            c.PostId=@postId and 
            c.Type='Comment' and 
            (c.PagerKey<@after or @after==null) 
            order by c.PagerKey desc");
            query.WithParameter("@postId", commentFindRequest.PostId);
            query.WithParameter("@after", commentFindRequest.NextPageToken);

            var iterator = Container.GetItemQueryIterator<Comment>(query);
            var result = new ObjectListResponse<Comment>();
            while (iterator.HasMoreResults)
            {
                var batch = await iterator.ReadNextAsync();
                foreach (var comment in batch)
                {
                    if (await userRepository.IsDeleted(comment.AuthorId))
                        continue;
                    result.NextPageToken = comment.PagingKey_CreatedAt_Id;
                    result.Items.Add(comment);
                    if (result.Items.Count >= 10)
                        return result;
                }
            }
            return result;
        }
    }
}