using System.Threading.Tasks;
using BandLab.Infrastructure;
using BandLab.Modules.Posts.Dto;
using BandLab.Modules.Posts.Dto.Post;
using BandLab.Modules.Posts.Model;
using Microsoft.Azure.Cosmos;

namespace BandLab.Modules.Posts.Logic
{
    public class PostRepository : Repository<Post>
    {
        private readonly UserRepository userRepository;

        public PostRepository(CosmosClient cosmosClient, UserRepository userRepository) : base(cosmosClient)
        {
            this.userRepository = userRepository;
        }

        public async Task Create(Post post)
        {
            await Container.UpsertItemAsync(post, new PartitionKey(post.PartitionKey));
        }

        public async Task<ObjectListResponse<Post>> GetPosts(PostFindRequest findRequest)
        {
            QueryDefinition query = new QueryDefinition(@"select * from Entities c  
            where 
            c.Type='Post' and 
            (c.AuthorId=@authorId or @authorId==null)and 
            (c.PagerKey<@after or @after==null) 
            order by c.PagerKey desc");
            
            query.WithParameter("@authorId", findRequest.AuthorId);
            query.WithParameter("@after", findRequest.NextPageToken);

            var iterator = Container.GetItemQueryIterator<Post>(query,null,new QueryRequestOptions()
            {
                PartitionKey = findRequest.AuthorId==null ? (PartitionKey?)null : new PartitionKey(findRequest.AuthorId)
            });
            var result = new ObjectListResponse<Post>();
            while (iterator.HasMoreResults)
            {
                var batch = await iterator.ReadNextAsync();
                foreach (var comment in batch)
                {
                    if (await userRepository.IsDeleted(comment.AuthorId))
                        continue;
                    result.NextPageToken = comment.PagingKey_CommentCount_Id;
                    result.Items.Add(comment);
                    if (result.Items.Count >= 10)
                        return result;
                }
            }
            return result;
        }

        public async Task<Post> Get(string postId)
        {
            return await this.Container.ReadItemAsync<Post>(postId, new PartitionKey(postId));
        }

        public async Task Update(Post post)
        {
            await Container.UpsertItemAsync(post, new PartitionKey(post.PartitionKey));
        }
    }
}