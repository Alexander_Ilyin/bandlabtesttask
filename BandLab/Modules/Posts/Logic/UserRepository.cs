using System.Threading.Tasks;
using BandLab.Infrastructure;
using Microsoft.Azure.Cosmos;
using User = BandLab.Modules.Posts.Model.User;

namespace BandLab.Modules.Posts.Logic
{
    public class UserRepository : Repository<User>
    {
        public UserRepository(CosmosClient cosmosClient) : base(cosmosClient)
        {
        }

        public async Task Delete(string userId)
        {
            var userResp = await Container.ReadItemAsync<User>(userId, new PartitionKey(userId));
            var user = userResp.Resource;
            await Container.UpsertItemAsync(userResp, new PartitionKey(user.Id));
        }

        public async Task<bool> IsDeleted(string userId)
        {
            var user = await Container.ReadItemAsync<User>(userId, new PartitionKey(userId));
            return user.Resource.IsDeleted;
        }

        public async Task Create(User user)
        {
            await Container.UpsertItemAsync(user, new PartitionKey(user.PartitionKey));
        }
    }
}