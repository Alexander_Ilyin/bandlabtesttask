using System;
using BandLab.Infrastructure;
using Newtonsoft.Json;

namespace BandLab.Modules.Posts.Model
{
    public class Comment : ModelBase
    {
        [JsonProperty("id")] public string Id { get; set; } = Guid.NewGuid().ToString();

        public string PostId { get; set; }
        public string AuthorId { get; set; }
        public string Text { get; set; }

        public string PagingKey_CreatedAt_Id => IndexFormatHelper.FormatDate(CreatedAt) + "_" + Id;
        public override string PartitionKey => this.PostId;
    }
}