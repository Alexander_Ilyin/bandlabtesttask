using System;
using System.Collections.Generic;
using System.Linq;
using BandLab.Infrastructure;

namespace BandLab.Modules.Posts.Model
{
    public class Post : ModelBase
    {
        
        public string Id { get; set; }
        public string AuthorId { get; set; }
        public string Text { get; set; }
        public List<string> UploadImageIds { get; set; } = new List<string>();
        
        public List<Comment> LastComments { get; set; } = new List<Comment>();
        public int CommentCount { get; set; }
        public override string PartitionKey => this.AuthorId;
        public string PagingKey_CommentCount_Id => IndexFormatHelper.FormatNumber(CommentCount) + "_" + Id;

        public void OnNewComment(Comment comment)
        {
            CommentCount++;
            LastComments.Add(comment);
            LastComments = LastComments.OrderByDescending(x => x.CreatedAt).Take(3).ToList();
        }

        public static string GenId(string authorId)
        {
            return authorId + "_" + Guid.NewGuid().ToString();
        }
    }
}