using System;
using System.Collections.Generic;
using BandLab.Infrastructure;

namespace BandLab.Modules.Posts.Model
{
    public class User : ModelBase
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string Email { get; set; }
        public bool IsDeleted { get; set; }
        public override string PartitionKey => this.Id;
        public void Delete()
        {
            IsDeleted = true;
        }
    }
}