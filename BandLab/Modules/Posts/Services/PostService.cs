using System.Linq;
using System.Threading.Tasks;
using BandLab.Infrastructure;
using BandLab.Modules.Posts.Dto.Comment;
using BandLab.Modules.Posts.Dto.Post;
using BandLab.Modules.Posts.Logic;
using BandLab.Modules.Posts.Model;

namespace BandLab.Modules.Posts.Services
{
    public class PostService
    {
        private readonly PostRepository postRepository;
        private readonly UserRepository userRepository;
        private readonly CommentRepository commentRepository;

        public PostService(PostRepository postRepository, UserRepository userRepository, CommentRepository commentRepository)
        {
            this.postRepository = postRepository;
            this.userRepository = userRepository;
            this.commentRepository = commentRepository;
        }

        public async Task<PostResponseItem> CreatePost(string currentUserId, PostCreateRequest req)
        {
            var newPost = new Post();
            newPost.Id = Post.GenId(currentUserId);
            newPost.AuthorId = currentUserId;
            newPost.Text = req.Text;
            newPost.UploadImageIds = req.UploadImageIds;
            await postRepository.Create(newPost);
            return PostResponseItem.From(newPost);
        }

        public async Task<CommentResponseItem> CreateComment(string currentUserId, CommentCreateRequest request)
        {
            var newComment = new Comment();
            newComment.Text = request.Text;
            newComment.PostId = request.PostId;
            newComment.AuthorId = currentUserId;
            await commentRepository.Create(newComment);

            var post = await postRepository.Get(request.PostId);
            post.OnNewComment(newComment);
            await postRepository.Update(post);

            return CommentResponseItem.From(newComment);
        }

        public async Task<ObjectListResponse<PostResponseItem>> GetPosts(PostFindRequest findRequest)
        {
            var posts = await postRepository.GetPosts(findRequest);
            return new ObjectListResponse<PostResponseItem>()
            {
                NextPageToken = posts.NextPageToken,
                Items = posts.Items.Select(PostResponseItem.From).ToList()
            };
        }

        public async Task<ObjectListResponse<CommentResponseItem>> GetComments(CommentFindRequest findRequest)
        {
            var comments = await commentRepository.GetComments(findRequest);
            return new ObjectListResponse<CommentResponseItem>()
            {
                NextPageToken = comments.NextPageToken,
                Items = comments.Items.Select(CommentResponseItem.From).ToList()
            };
        }
    }
}