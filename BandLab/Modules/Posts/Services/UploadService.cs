using System.IO;
using System.Threading.Tasks;
using BandLab.Infrastructure;

namespace BandLab.Modules.Posts.Services
{
    public class UploadService
    {
        private readonly BlobStorage storage;

        public UploadService(BlobStorage storage)
        {
            this.storage = storage;
        }

        public Task<string> AddImage(Stream stream)
        {
            var memoryStream = new MemoryStream();
            ImageConverter.ConvertToJpg(stream, memoryStream);
            return storage.AddBlob( memoryStream.ToArray());
        }
    }
}