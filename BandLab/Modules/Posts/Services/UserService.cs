using System;
using System.Threading.Tasks;
using BandLab.Modules.Posts.Dto.User;
using BandLab.Modules.Posts.Logic;
using BandLab.Modules.Posts.Model;

namespace BandLab.Modules.Posts.Services
{
    public class UserService
    {
        private readonly UserRepository userRepository;

        public UserService(UserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public async Task<UserResponseItem> CreateUser(string authorId, UserCreateRequest req)
        {
            var user = new User();
            user.Email = req.Email;

            await userRepository.Create(user);
            return UserResponseItem.From(user);
        }
        
        public async Task DeleteUser(string currentUserId, string userId)
        {
            if (currentUserId != userId)
                throw new Exception("Access denied");
            await userRepository.Delete(userId);
        }
    }
}